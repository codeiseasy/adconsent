package ma.allovacances.app.util

import android.content.Context
import android.graphics.Typeface
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

object FontUtil {

    fun overrideFonts(context: Context?, v: View, path: String) {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    overrideFonts(context, v.getChildAt(i), path)
                }
            } else if (v is TextView) {
                v.typeface = Typeface.createFromAsset(context!!.assets, path)
            }
        } catch (e: Exception) {
        }
    }

    fun overrideFonts(context: Context?, v: Array<View>, path: String) {
        try {
            for (a in 0 until v.size){
                var view = v[a]
                if (view is ViewGroup) {
                    for (i in 0 until view.childCount) {
                        overrideFonts(context, view.getChildAt(i), path)
                    }
                } else if (view is TextView) {
                    view.typeface = Typeface.createFromAsset(context!!.assets, path)
                }
            }

        } catch (e: Exception) {
        }
    }

    fun overrideFonts(context: Context?, textView: Array<TextView>, path: String) {
        try {
            for (i in 0 until textView.size){
                textView[i].typeface = Typeface.createFromAsset(context!!.assets, path)
            }
        } catch (e: Exception) {
        }
    }


    fun overrideSizes(context: Context?, v: View, size: Double) {
        try {
            if (v is ViewGroup) {
                for (i in 0 until v.childCount) {
                    overrideSizes(context, v.getChildAt(i), size)
                }
            } else if (v is TextView) {
                v.textSize = size.toFloat()
            }
        } catch (e: Exception) {
        }

    }
}
