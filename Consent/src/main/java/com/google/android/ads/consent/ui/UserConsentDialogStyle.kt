package com.google.android.ads.consent.ui

import android.content.Context
import android.graphics.Color
import com.google.android.ads.consent.R


class UserConsentDialogStyle(context: Context?) {
    private var displayIcon: Boolean = false
    private var multipleColors: Boolean = false
    private var btnMultipleColors: Boolean = false

    private var cornerRadius: Int = 0
    private var btnCornerRadius: Int = 1

    private var dialogTitleColor: Int = Color.WHITE //EC8232
    private var dialogImgBackColor: Int = Color.WHITE //EC8232
    private var dialogTextColor: Int = Color.parseColor("#45494A") //EC8232

    private var dialogBackgroundColor: Int = Color.WHITE //EC8232
    private var topOfDialogBackgroundColor: Int = Color.parseColor("#293441") //EC8232
    private var topOfDialogBackgroundColors: IntArray? = null

    private var dialogLinksColor: Int = Color.parseColor("#2781D2") //EC8232

    private var btnPersonalizedIAgreeText: String = context!!.resources.getString(R.string.eu_consent_personalized_agree)
    private var btnNonPersonalizedDisagreeText: String = context!!.resources.getString(R.string.btn_eu_consent_disagree)
    private var btnNonPersonalizedAgreeText: String = context!!.resources.getString(R.string.eu_consent_non_personalized_agree)

    private var btnBackgroundColor: Int = Color.parseColor("#0cc90c")
    private var btnBackgroundColors: IntArray? = null
    private var btnTextColor: Int = Color.WHITE


    private var topOfDialogTitleFontPath: String = "fonts/Roboto-Regular.ttf"
    private var dialogContentFontPath: String = "fonts/Roboto-Regular.ttf"


    fun setDisplayIcon(display: Boolean){
        displayIcon = display
    }

    fun isDiasplayicon(): Boolean {
        return displayIcon
    }

    fun setTopOfDialogTitleColor(color: Int){
        dialogTitleColor = color
        dialogImgBackColor = color
    }

    fun getTopOfDialogTitleColor(): Int {
        return dialogTitleColor
    }

    fun getDialogImgBackColor(): Int {
        return dialogImgBackColor
    }


    fun setDialogContentTextColor(color: Int){
        dialogTextColor = color
    }

    fun getDialogContentTextColor(): Int {
        return dialogTextColor
    }


    fun setDialogLinksColor(color: Int){
        dialogLinksColor = color
    }

    fun getDialogLinksColor(): Int {
        return dialogLinksColor
    }

    fun setBtnTextColor(color: Int){
        btnTextColor = color
    }

    fun getBtnTextColor(): Int {
        return btnTextColor
    }

    fun setTopOfDialogBackgroundColor(color: Int){
        topOfDialogBackgroundColor = color
        multipleColors = false
    }

    fun setTopOfDialogBackgroundColors(colors: IntArray){
        topOfDialogBackgroundColors = colors
        multipleColors = true
    }

    fun setDialogBackgroundColor(color: Int){
        dialogBackgroundColor = color
    }

    fun getDialogBackgroundColor(): Int {
        return dialogBackgroundColor
    }

    fun setBtnBackgroundColor(colors: Int){
        btnBackgroundColor = colors
        btnMultipleColors = false
    }

    fun isBtnMultipleColors(): Boolean {
        return btnMultipleColors
    }

    fun setBtnBackgroundColors(colors: IntArray){
        btnBackgroundColors = colors
        btnMultipleColors = true
    }

    fun setDialogCornerRadius(corner: Int){
        cornerRadius = corner
    }

    fun getDialogCornerRadius(): Int{
         return cornerRadius
    }

    fun getTopOfDialogBackgroundColor(): Int{
        return topOfDialogBackgroundColor
    }

    fun getTopOfDialogBackgroundColors(): IntArray{
        return topOfDialogBackgroundColors!!
    }

    fun getBtnBackgroundColor(): Int {
        return btnBackgroundColor!!
    }

    fun getBtnBackgroundColors(): IntArray {
        return btnBackgroundColors!!
    }

    fun setTopOfDialogTitleFont(path: String){
        topOfDialogTitleFontPath = path
    }

    fun getTopOfDialogTitleFont(): String {
        return topOfDialogTitleFontPath
    }

    fun setDialogContentFont(path: String){
        dialogContentFontPath = path
    }

    fun getDialogContentFont(): String {
        return dialogContentFontPath
    }


    fun setBtnPersonalizedIAgreeText(text: String){
        btnPersonalizedIAgreeText = text
    }

    fun getBtnPersonalizedIAgreeText(): String{
        return btnPersonalizedIAgreeText
    }

    fun setBtnPersonalizedDisagreeText(text: String){
        btnNonPersonalizedDisagreeText = text
    }

    fun getBtnPersonalizedDisagreeText(): String {
        return btnNonPersonalizedDisagreeText
    }

    fun setBtnPersonalizedAgreeText(text: String){
        btnNonPersonalizedAgreeText = text
    }

    fun getBtnPersonalizedAgreeText(): String {
        return btnNonPersonalizedAgreeText
    }

    fun setBtnCornerRadius(corner: Int){
        btnCornerRadius = corner
    }


    fun getBtnCornerRadius(): Int {
        return btnCornerRadius
    }


    fun isMultipleColors(): Boolean{
        return multipleColors
    }


}