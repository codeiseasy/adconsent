package com.google.android.ads.consent.enums

enum class WindowMode {
    DIALOG,
    SHEET_BOTTOM
}