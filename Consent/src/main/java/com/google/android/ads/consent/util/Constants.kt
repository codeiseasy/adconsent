package com.google.android.ads.consent.util

object Constants {
    const val extraKeyPrivacyUrl = "extra_key_privacy_url"
    const val extraKeyPublisherId = "extra_key_publisher_id"
}
