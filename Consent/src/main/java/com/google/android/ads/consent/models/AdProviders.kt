package com.google.android.ads.consent.models

class AdProviders(private val name: String?, private val link: String?) {

    fun getName(): String? {
        return name
    }

    fun getLink(): String? {
        return link
    }

}