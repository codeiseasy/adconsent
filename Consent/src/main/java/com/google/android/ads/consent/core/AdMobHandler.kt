package com.google.android.ads.consent.core

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.easyapps.chwafa.util.ads.NativeAdvancedAd
import com.google.android.ads.consent.R
import com.google.android.ads.consent.listener.abstracts.AdMobListener
import com.google.android.ads.consent.listener.abstracts.NativeAdListener
import com.google.android.ads.consent.listener.enums.CustomAdSize
import com.google.android.ads.consent.listener.interfaces.AdAsyncCallback
import com.google.android.ads.consent.listener.interfaces.AdRewardedListener
import com.google.android.ads.consent.ui.UserConsent
import com.google.android.ads.consent.util.AdvertisePreferences
import com.google.android.ads.consent.util.CheckNetwork
import com.google.android.ads.consent.util.LogDebug
import com.google.android.ads.consent.widget.CustomAdView
import com.google.android.ads.nativetemplates.*
import com.google.android.gms.ads.*
import com.google.android.gms.ads.formats.*

import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.wang.avi.AVLoadingIndicatorView
import java.util.*


open class AdMobHandler(private var context: Context?, private val adRequest: AdRequest) {
    private var advertisePreferences: AdvertisePreferences? = null

    private var isDebugTestAds: Boolean = false
    private var isCustomAdView: Boolean = false

    private var adAppUnitId: String? = null
    private var adViewUnitId: String? = null
    private var adInterstitialUnitId: String? = null
    private var adRewardedVideoUnitId: String? = null
    private var adNativeAdvancedUnitId: String? = null
    private var adNativeAdvancedVideoUnitId: String? = null

    var interstitialAd: InterstitialAd? = null
    var rewardedVideoAd: RewardedVideoAd? = null
    var adViewBanner: AdView? = null
    private var adBannerSize: AdSize? = null

    private var customAdView: CustomAdView? = null
    private var adMobListener: AdMobListener? = null

    private var layoutInflater: LayoutInflater
    private var displayMetrics: DisplayMetrics

    private var nativeAppInstallAd: NativeAppInstallAd? = null
    private var nativeContentAd: NativeContentAd? = null
    private var unifiedNativeAd: TemplateView? = null

    private fun dimension(value: Int): Float {
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            value.toFloat(),
            displayMetrics
        )
    }

    init {
        if (adRequest == null) {
            throw IllegalStateException("adRequest $initError")
        }
        layoutInflater = LayoutInflater.from(context)
        displayMetrics = context!!.resources.displayMetrics

        advertisePreferences = AdvertisePreferences(context)
        interstitialAd = InterstitialAd(context)
        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(context)
        adViewBanner = AdView(context)
        customAdView = CustomAdView(context)
    }

    fun setAppUnitId(adUnitId: String?) {
        adAppUnitId = if (isDebugTestAds) {
            adTestAppUnitId
        } else {
            adUnitId
        }
        if (adAppUnitId == null) {
            throw IllegalStateException("app unit id $initError")
        }
        MobileAds.initialize(context, adAppUnitId)
    }

    fun setAdViewUnitId(adUnitId: String?) {
        adViewUnitId = if (isDebugTestAds) {
            adTestBannerId
        } else {
            adUnitId
        }
        adViewBanner?.adUnitId = adViewUnitId
        customAdView?.setAdUnitId(adViewUnitId)
    }

    fun setInterstitialUnitId(adUnitId: String?) {
        adInterstitialUnitId = if (isDebugTestAds) {
            adTestInterstitialId
        } else {
            adUnitId
        }
        interstitialAd?.adUnitId = adInterstitialUnitId
    }

    fun setRewardedVideoUnitId(adUnitId: String?) {
        adRewardedVideoUnitId = if (isDebugTestAds) {
            adTestRewardedVideoId
        } else {
            adUnitId
        }
    }

    fun setNativeAdvancedUnitId(adUnitId: String?) {
        adNativeAdvancedUnitId = if (isDebugTestAds) {
            adTestNativeAdvancedId
        } else {
            adUnitId
        }
    }

    fun setNativeAdvancedVideoUnitId(adUnitId: String?) {
        adNativeAdvancedVideoUnitId = if (isDebugTestAds) {
            adTestNativeAdvancedVideoId
        } else {
            adUnitId
        }
    }

    fun setAdViewSize(adSize: AdSize?) {
        adBannerSize = adSize
    }

    fun setCustomAdViewSize(customAdSize: CustomAdSize?) {
        adBannerSize = when (customAdSize!!) {
            CustomAdSize.BANNER -> AdSize.BANNER
            CustomAdSize.SMART_BANNER -> AdSize.SMART_BANNER
            CustomAdSize.LARGE_BANNER -> AdSize.LARGE_BANNER
            CustomAdSize.FULL_BANNER -> AdSize.FULL_BANNER
            CustomAdSize.MEDIUM_RECTANGLE -> AdSize.MEDIUM_RECTANGLE
        }
    }

    fun loadAdView(adView: AdView, mobListener: AdMobListener?) {
        adMobListener = mobListener
        isCustomAdView = false
        adViewBanner = adView
        adViewListener(adMobListener)
    }

    fun loadCustomAdView(customView: CustomAdView, mobListener: AdMobListener?) {
        adMobListener = mobListener
        customAdView = customView
        isCustomAdView = true
        customView.loadAd(adRequest, adViewUnitId, adBannerSize)
    }

    fun loadAdView(layoutAd: LinearLayout) {
        if (adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        adViewBanner = AdView(context)
        adViewBanner!!.adSize = adBannerSize
        adViewBanner!!.adUnitId = adViewUnitId
        layoutAd.gravity = Gravity.CENTER_HORIZONTAL
        layoutAd.addView(adViewBanner)
        adViewBanner!!.loadAd(adRequest)
    }

    fun loadAdView(layoutAd: LinearLayout, mobListener: AdMobListener?) {
        if (adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        var adContainer = layoutAd
        adViewBanner = AdView(context)
        adViewBanner!!.adUnitId = adViewUnitId
        adViewBanner!!.adSize = adBannerSize
        adViewBanner!!.loadAd(adRequest)
        adViewBanner!!.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mobListener?.onAdLoaded()
            }

            override fun onAdOpened() {
                super.onAdOpened()
                mobListener?.onAdOpened()
            }

            override fun onAdImpression() {
                super.onAdImpression()
                mobListener?.onAdImpression()
            }

            override fun onAdClicked() {
                super.onAdClicked()
                mobListener?.onAdClicked()
            }

            override fun onAdClosed() {
                super.onAdClosed()
                mobListener?.onAdClosed()
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                mobListener?.onAdLeftApplication()
            }

            override fun onAdFailedToLoad(p0: Int) {
                super.onAdFailedToLoad(p0)
                mobListener?.onAdFailedToLoad(p0)
            }
        }

        val params: LinearLayout.LayoutParams = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        adContainer.addView(adViewBanner, params)
    }

    fun loadAdView(layoutAd: RelativeLayout) {
        if (adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        var adContainer = layoutAd
        adViewBanner = AdView(context)

        val params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        //params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        adViewBanner?.layoutParams = params

        adViewBanner!!.id = R.id.ad_view_banner
        adViewBanner!!.adSize = adBannerSize
        adViewBanner!!.adUnitId = adViewUnitId
        adContainer.removeView(adViewBanner)
        adContainer.addView(adViewBanner)
        adViewBanner!!.loadAd(adRequest)
    }

    fun loadAdView(layoutAd: RelativeLayout, mobListener: AdMobListener?) {
        if (adViewUnitId == null) {
            throw IllegalStateException("ad unitId $initError")
        }
        if (adBannerSize == null) {
            throw IllegalStateException("ad size $initError")
        }
        var adContainer = layoutAd
        adViewBanner = AdView(context)

        val params: RelativeLayout.LayoutParams = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.WRAP_CONTENT,
            RelativeLayout.LayoutParams.WRAP_CONTENT
        )
        params.addRule(RelativeLayout.CENTER_IN_PARENT)
        //params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        adViewBanner?.layoutParams = params

        adViewBanner!!.id = R.id.ad_view_banner
        adViewBanner!!.adSize = adBannerSize
        adViewBanner!!.adUnitId = adViewUnitId
        adContainer.removeView(adViewBanner)
        adContainer.addView(adViewBanner)
        adViewBanner!!.loadAd(adRequest)
        adViewBanner!!.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mobListener?.onAdLoaded()
            }

            override fun onAdOpened() {
                super.onAdOpened()
                mobListener?.onAdOpened()
            }

            override fun onAdImpression() {
                super.onAdImpression()
                mobListener?.onAdImpression()
            }

            override fun onAdClicked() {
                super.onAdClicked()
                mobListener?.onAdClicked()
            }

            override fun onAdClosed() {
                super.onAdClosed()
                mobListener?.onAdClosed()
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                mobListener?.onAdLeftApplication()
            }

            override fun onAdFailedToLoad(p0: Int) {
                super.onAdFailedToLoad(p0)
                mobListener?.onAdFailedToLoad(p0)
            }
        }
    }

    fun loadInterstitialAd() {
        if (adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        interstitialAdListener(null)
    }

    fun loadInterstitialAd(callback: AdMobListener?) {
        if (adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        interstitialAdListener(callback)
    }

    fun loadInterstitialAd(unitIdRes: String, callback: AdMobListener?) {
        setInterstitialUnitId(unitIdRes)

        if (adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        interstitialAdListener(callback)
    }

    fun loadInterstitialAd(@StringRes unitIdRes: Int, callback: AdMobListener?) {
        setInterstitialUnitId(context!!.resources.getString(unitIdRes))

        if (adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialAd before loadAd is called.")
        }
        interstitialAdListener(callback)
    }

    fun loadAdRewardedVideo() {
        if (adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        rewardedVideoListener(null)
    }

    fun loadAdRewardedVideo(listener: AdRewardedListener?) {
        if (adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        rewardedVideoListener(listener)
    }

    fun loadAdRewardedVideo(unitIdRes: String, listener: AdRewardedListener?) {
        setRewardedVideoUnitId(unitIdRes)

        if (adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        rewardedVideoListener(listener)
    }

    fun loadAdRewardedVideo(@StringRes unitIdRes: Int, listener: AdRewardedListener?) {
        setRewardedVideoUnitId(context!!.resources.getString(unitIdRes))
        if (adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        rewardedVideoListener(listener)
    }

    fun loadInterstitialAfterCountOf(key: String?, count: Int, callback: AdAsyncCallback?) {
        if (adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }
        var totalCount = doCountInterstitialAd(key)
        if (CheckNetwork.getInstance(context).isOnline) {
            advertisePreferences?.setAdCountPref(key, totalCount)
            if (advertisePreferences!!.getAdCountPref(key) == count) {
                advertisePreferences?.setAdCountPref(key, 0)
                if (CheckNetwork.getInstance(context).isOnline) {
                    interstitialAdListener(object : AdMobListener() {
                        override fun onAdClosed() {
                            super.onAdClosed()
                            callback?.onNext()
                        }

                        override fun onAdFailedToLoad(var1: Int) {
                            super.onAdFailedToLoad(var1)
                            callback?.onNext()
                        }
                    })
                } else {
                    callback?.onNext()
                }
            } else {
                callback?.onNext()
            }
        } else {
            callback?.onNext()
        }
    }

    fun loadInterstitialAfterCountOf(key: String?, count: Int) {
        if (adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }
        var totalCount = doCountInterstitialAd(key)
        if (CheckNetwork.getInstance(context).isOnline) {
            if (advertisePreferences!!.getAdCountPref(key) == count) {
                advertisePreferences?.setAdCountPref(key, 1)
                interstitialAdListener(null)
            } else {
                advertisePreferences?.setAdCountPref(key, totalCount)

            }
        }
        LogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    fun loadInterstitialAfterCountOf(key: String?, count: Int, callback: AdMobListener) {
        if (adInterstitialUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on InterstitialUnitId before loadAd is called.")
        }
        var totalCount = doCountInterstitialAd(key)
        if (CheckNetwork.getInstance(context).isOnline) {
            if (advertisePreferences!!.getAdCountPref(key) == count) {
                advertisePreferences?.setAdCountPref(key, 1)
                interstitialAdListener(object : AdMobListener() {
                    override fun onAdLoaded() {
                        super.onAdLoaded()
                        callback?.onAdLoaded()
                    }

                    override fun onAdOpened() {
                        super.onAdOpened()
                        callback?.onAdOpened()
                    }

                    override fun onAdClosed() {
                        super.onAdClosed()
                        callback?.onAdClosed()
                    }

                    override fun onAdFailedToLoad(var1: Int) {
                        super.onAdFailedToLoad(var1)
                        callback?.onAdFailedToLoad(var1)
                    }

                    override fun onAdClicked() {
                        super.onAdClicked()
                        callback?.onAdClicked()
                    }

                    override fun onAdImpression() {
                        super.onAdImpression()
                        callback?.onAdImpression()
                    }

                    override fun onAdLeftApplication() {
                        super.onAdLeftApplication()
                        callback?.onAdLeftApplication()
                    }
                })
            } else {
                advertisePreferences?.setAdCountPref(key, totalCount)
                callback?.onAdClosed()
            }
        } else {
            callback?.onAdClosed()
        }
        LogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }


    fun loadRewardedVideoAfterCountOf(key: String, count: Int) {
        if (adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        var totalCount = doCountRewardedVideo(key)
        if (CheckNetwork.getInstance(context).isOnline) {
            if (advertisePreferences!!.getAdCountPref(key) == count) {
                advertisePreferences?.setAdCountPref(key, 1)
                rewardedVideoListener(null)
            } else {
                advertisePreferences!!.setAdCountPref(key, totalCount)

            }
        }
        LogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    fun loadRewardedVideoAfterCountOf(key: String, count: Int, listener: AdRewardedListener?) {
        if (adRewardedVideoUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on RewardedVideoUnitId before loadAd is called.")
        }
        var totalCount = doCountRewardedVideo(key)
        if (CheckNetwork.getInstance(context).isOnline) {
            if (advertisePreferences!!.getAdCountPref(key) == count) {
                advertisePreferences?.setAdCountPref(key, 1)
                rewardedVideoListener(listener)
            } else {
                advertisePreferences!!.setAdCountPref(key, totalCount)
                listener?.onRewardedVideoAdClosed()
            }
        } else {
            listener?.onRewardedVideoAdClosed()
        }
        LogDebug.d("LOAD_AD_AFTER_COUNTDOWN", totalCount.toString())
    }

    private fun doCountInterstitialAd(key: String?): Int {
        return advertisePreferences!!.getAdCountPref(key) + 1
    }

    private fun doCountRewardedVideo(key: String?): Int {
        return advertisePreferences!!.getAdCountPref(key) + 1
    }

    private fun adViewListener(mobListener: AdMobListener?) {
        //adViewBanner?.adSize = adBannerSize
        if (isCustomAdView!!) {
            customAdView?.addView(adViewBanner, 0)
        }

        adViewBanner?.loadAd(adRequest)
        adViewBanner?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mobListener?.onAdLoaded()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdLoaded()}]")

            }

            override fun onAdOpened() {
                super.onAdOpened()
                mobListener?.onAdOpened()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdOpened()}]")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                mobListener?.onAdClicked()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdClicked()}]")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                mobListener?.onAdClosed()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdClosed()}]")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                mobListener?.onAdImpression()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdImpression()}]")
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                mobListener?.onAdLeftApplication()
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdLeftApplication()}]")
            }

            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                mobListener?.onAdFailedToLoad(i)
                LogDebug.d(TAG, "[loadCustomAdView(){\nonAdFailedToLoad($i)}]")
            }
        }
    }

    private fun interstitialAdListener(callback: AdMobListener?) {
        interstitialAd?.loadAd(adRequest)
        interstitialAd?.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                callback?.onAdLoaded()
                if (interstitialAd!!.isLoaded) {
                    interstitialAd?.show()
                }
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdLoaded()}]")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                callback?.onAdClicked()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdClicked()}]")
            }

            override fun onAdOpened() {
                super.onAdOpened()
                callback?.onAdOpened()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdOpened()}]")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                callback?.onAdClosed()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdClosed()}]")
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                callback?.onAdLeftApplication()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdLeftApplication()}]")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                callback?.onAdImpression()
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdImpression()}]")
            }

            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                callback?.onAdFailedToLoad(i)
                LogDebug.d(TAG, "[loadInterstitialAd(){\nonAdFailedToLoad($i)}]")
            }

        }
    }

    private fun rewardedVideoListener(listener: AdRewardedListener?) {
        rewardedVideoAd?.loadAd(adRewardedVideoUnitId, adRequest)
        rewardedVideoAd?.rewardedVideoAdListener = object : RewardedVideoAdListener {

            override fun onRewardedVideoAdLoaded() {
                listener?.onRewardedVideoAdLoaded()
                if (rewardedVideoAd!!.isLoaded) {
                    rewardedVideoAd?.show()
                }
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdLoaded()}]")
            }

            override fun onRewardedVideoAdOpened() {
                listener?.onRewardedVideoAdOpened()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdOpened()}]")
            }

            override fun onRewardedVideoStarted() {
                listener?.onRewardedVideoStarted()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoStarted()}]")
            }

            override fun onRewardedVideoAdClosed() {
                listener?.onRewardedVideoAdClosed()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdClosed()}]")
            }

            override fun onRewarded(rewardItem: RewardItem) {
                listener?.onRewarded(rewardItem)
                LogDebug.d(
                    TAG,
                    "[loadAdRewardedVideo(){\nonRewarded(${rewardItem.type + " : " + rewardItem.amount})}]"
                )
            }

            override fun onRewardedVideoAdLeftApplication() {
                listener?.onRewardedVideoAdLeftApplication()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdLeftApplication()}]")
            }

            override fun onRewardedVideoAdFailedToLoad(i: Int) {
                listener?.onRewardedVideoAdFailedToLoad(i)
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoAdFailedToLoad($i)}]")
            }

            override fun onRewardedVideoCompleted() {
                listener?.onRewardedVideoCompleted()
                LogDebug.d(TAG, "[loadAdRewardedVideo(){\nonRewardedVideoCompleted()}]")
            }
        }
    }


    private fun populateAppInstallAdView(
        nativeAppInstallAd: NativeAppInstallAd,
        nativeAppInstallAdView: NativeAppInstallAdView
    ) {
        //this.nativeAppInstallAd?.destroy()
        this.nativeAppInstallAd = nativeAppInstallAd

        val videoController = nativeAppInstallAd.videoController
        videoController.videoLifecycleCallbacks =
            object : VideoController.VideoLifecycleCallbacks() {
                override fun onVideoEnd() {
                    super.onVideoEnd()
                }
            }
        nativeAppInstallAdView.headlineView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_headline)
        nativeAppInstallAdView.bodyView = nativeAppInstallAdView.findViewById(R.id.appinstall_body)
        nativeAppInstallAdView.callToActionView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_call_to_action)
        nativeAppInstallAdView.iconView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_app_icon)
        nativeAppInstallAdView.priceView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_price)
        nativeAppInstallAdView.starRatingView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_stars)
        nativeAppInstallAdView.storeView =
            nativeAppInstallAdView.findViewById(R.id.appinstall_store)
        (nativeAppInstallAdView.headlineView as TextView).text = nativeAppInstallAd.headline
        (nativeAppInstallAdView.bodyView as TextView).text = nativeAppInstallAd.body
        (nativeAppInstallAdView.callToActionView as Button).text = nativeAppInstallAd.callToAction
        (nativeAppInstallAdView.iconView as ImageView).setImageDrawable(nativeAppInstallAd.icon.drawable)
        val mediaView = nativeAppInstallAdView.findViewById<MediaView>(R.id.appinstall_media)
        val imageView = nativeAppInstallAdView.findViewById<ImageView>(R.id.appinstall_image)
        if (videoController.hasVideoContent()) {
            nativeAppInstallAdView.mediaView = mediaView
            imageView.visibility = View.GONE
        } else {
            nativeAppInstallAdView.imageView = imageView
            mediaView.visibility = View.GONE
            val images = nativeAppInstallAd.images
            if (images.size > 0) {
                imageView.setImageDrawable(images[0].drawable)
            }
        }
        if (nativeAppInstallAd.price == null) {
            nativeAppInstallAdView.priceView.visibility = View.INVISIBLE
        } else {
            nativeAppInstallAdView.priceView.visibility = View.VISIBLE
            (nativeAppInstallAdView.priceView as TextView).text = nativeAppInstallAd.price
        }
        if (nativeAppInstallAd.store == null) {
            nativeAppInstallAdView.storeView.visibility = View.INVISIBLE
        } else {
            nativeAppInstallAdView.storeView.visibility = View.VISIBLE
            (nativeAppInstallAdView.storeView as TextView).text = nativeAppInstallAd.store
        }
        if (nativeAppInstallAd.starRating == null) {
            nativeAppInstallAdView.starRatingView.visibility = View.INVISIBLE
        } else {
            (nativeAppInstallAdView.starRatingView as RatingBar).rating =
                nativeAppInstallAd.starRating.toFloat()
            nativeAppInstallAdView.starRatingView.visibility = View.VISIBLE
        }
        nativeAppInstallAdView.setNativeAd(nativeAppInstallAd)
    }

    private fun populateContentAdView(
        nativeContentAd: NativeContentAd,
        nativeContentAdView: NativeContentAdView
    ) {
        //this.nativeContentAd?.destroy()
        this.nativeContentAd = nativeContentAd

        nativeContentAd.destroy()
        nativeContentAdView.headlineView = nativeContentAdView.findViewById(R.id.contentad_headline)
        nativeContentAdView.imageView = nativeContentAdView.findViewById(R.id.contentad_image)
        nativeContentAdView.bodyView = nativeContentAdView.findViewById(R.id.contentad_body)
        nativeContentAdView.callToActionView =
            nativeContentAdView.findViewById(R.id.contentad_call_to_action)
        nativeContentAdView.logoView = nativeContentAdView.findViewById(R.id.contentad_logo)
        nativeContentAdView.advertiserView =
            nativeContentAdView.findViewById(R.id.contentad_advertiser)
        (nativeContentAdView.headlineView as TextView).text = nativeContentAd.headline
        (nativeContentAdView.bodyView as TextView).text = nativeContentAd.body
        (nativeContentAdView.callToActionView as TextView).text = nativeContentAd.callToAction
        (nativeContentAdView.advertiserView as TextView).text = nativeContentAd.advertiser
        val images = nativeContentAd.images
        if (images.size > 0) {
            (nativeContentAdView.imageView as ImageView).setImageDrawable(
                images[0].drawable
            )
        }
        val logo = nativeContentAd.logo
        if (logo == null) {
            nativeContentAdView.logoView.visibility = View.INVISIBLE
        } else {
            (nativeContentAdView.logoView as ImageView).setImageDrawable(logo.drawable)
            nativeContentAdView.logoView.visibility = View.VISIBLE
        }
        nativeContentAdView.setNativeAd(nativeContentAd)
    }

    fun loadNativeAd(unitAdView: ViewGroup) {
        this.loadNativeAd(unitAdView, null)
    }

    fun loadNativeAd(unitAdView: ViewGroup, adListener: NativeAdListener?) {
        this.refreshNativeAd(
            unitAdView,
            AdDisplayMode.CONTENT,
            R.layout.ad_medium_unified_native,
            0,
            adListener
        )
    }

    fun loadNativeAd(unitAdView: ViewGroup, displayMode: AdDisplayMode) {
        this.loadNativeAd(unitAdView, displayMode, null)
    }

    fun loadNativeAd(
        unitAdView: ViewGroup,
        displayMode: AdDisplayMode,
        adListener: NativeAdListener?
    ) {
        this.refreshNativeAd(
            unitAdView,
            displayMode,
            R.layout.ad_medium_unified_native,
            0,
            adListener
        )
    }

    fun loadUnifiedNativeAd(unitAdView: ViewGroup, size: AdDisplaySize) {
        this.loadUnifiedNativeAd(unitAdView, size, null)
    }

    fun loadUnifiedNativeAd(
        unitAdView: ViewGroup,
        size: AdDisplaySize,
        adListener: NativeAdListener?
    ) {
        when (size) {
            AdDisplaySize.SMALL -> refreshNativeAd(
                unitAdView,
                AdDisplayMode.UNIFIED_NATIVE,
                R.layout.ad_small_unified_native,
                0,
                adListener
            )
            AdDisplaySize.MEDIUM -> refreshNativeAd(
                unitAdView,
                AdDisplayMode.UNIFIED_NATIVE,
                R.layout.ad_medium_unified_native,
                0,
                adListener
            )
        }
    }

    fun loadNativeAd(unitAdView: ViewGroup, displayMode: AdDisplayMode, @AdTypeRange adType: Int) {
        refreshNativeAd(unitAdView, displayMode, 0, 0, null)
    }


    fun loadNativeForAppInstallAd(unitAdView: ViewGroup) {
        this.loadNativeForAppInstallAd(unitAdView, null)
    }

    fun loadNativeForAppInstallAd(unitAdView: ViewGroup, adListener: NativeAdListener?) {
        var params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.FILL_PARENT,
            dimension(190).toInt()
        )
        var loadingView = LayoutInflater.from(context).inflate(R.layout.ad_loading_layout, null)
        var avLoadingIndicatorView =
            loadingView.findViewById<AVLoadingIndicatorView>(R.id.ad_loading_view)
        if (avLoadingIndicatorView != null) {
            avLoadingIndicatorView!!.setIndicator(
                NativeAdvancedAd(context!!).loadingAnimationType[Random().nextInt(
                    NativeAdvancedAd(context!!).loadingAnimationType.size
                )]
            )
        }
        unitAdView.removeAllViews()
        unitAdView.addView(loadingView, params)

        val videoOptions = VideoOptions.Builder().setStartMuted(true).build()
        var adOptions = NativeAdOptions.Builder().setVideoOptions(videoOptions).build()

        val builder = AdLoader.Builder(context, adNativeAdvancedUnitId)
            .forAppInstallAd { nativeAppInstallAd ->
                LogDebug.d("forAppInstallAd", "load native ad")
                val nativeAppInstallAdView = layoutInflater.inflate(
                    R.layout.ad_app_install,
                    null
                ) as NativeAppInstallAdView
                populateAppInstallAdView(nativeAppInstallAd, nativeAppInstallAdView)
                unitAdView.removeAllViews()
                unitAdView.addView(nativeAppInstallAdView)
            }.forContentAd { nativeContentAd ->
                val nativeContentAdView =
                    layoutInflater.inflate(R.layout.ad_content, null) as NativeContentAdView
                populateContentAdView(nativeContentAd, nativeContentAdView)
                unitAdView.removeAllViews()
                unitAdView.addView(nativeContentAdView)
            }.forUnifiedNativeAd { unifiedNativeAd ->
                var colorDrawable =
                    ColorDrawable(ContextCompat.getColor(context!!, R.color.white))
                val nativeContentAdView =
                    layoutInflater.inflate(
                        R.layout.ad_medium_unified_native,
                        null
                    ) as TemplateView // ad_small_unified_native
                val template: TemplateView = nativeContentAdView.findViewById(R.id.my_template)
                val styles = NativeTemplateStyle.Builder()
                    .withMainBackgroundColor(colorDrawable)
                    .build()
                template.setStyles(styles)
                template.setNativeAd(unifiedNativeAd)
                unitAdView.removeAllViews()
                unitAdView.addView(nativeContentAdView)
                this.unifiedNativeAd = template
            }
            .withNativeAdOptions(adOptions)
            .withAdListener(object : AdListener() {
                override fun onAdLoaded() {
                    super.onAdLoaded()
                    adListener?.onAdLoaded()
                    unitAdView.visibility = View.VISIBLE
                    avLoadingIndicatorView?.visibility = View.GONE
                }

                override fun onAdOpened() {
                    super.onAdOpened()
                    adListener?.onAdOpened()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    adListener?.onAdClosed()
                }

                override fun onAdClicked() {
                    super.onAdClicked()
                    adListener?.onAdClicked()
                }

                override fun onAdFailedToLoad(errorCode: Int) {
                    super.onAdFailedToLoad(errorCode)
                    adListener?.onAdFailedToLoad(errorCode)
                    LogDebug.d("AdFailedToLoad", "Failed to load native ad: $errorCode")
                    unitAdView.visibility = View.GONE
                    avLoadingIndicatorView?.visibility = View.GONE
                }

                override fun onAdImpression() {
                    super.onAdImpression()
                    adListener?.onAdImpression()
                }

                override fun onAdLeftApplication() {
                    super.onAdLeftApplication()
                    adListener?.onAdLeftApplication()
                }
            }).build().loadAd(adRequest)
    }

    private fun refreshNativeAd(
        unitAdView: ViewGroup,
        displayMode: AdDisplayMode?, @LayoutRes layoutRes: Int, @AdTypeRange adType: Int,
        adListener: NativeAdListener?
    ) {
        if (adNativeAdvancedUnitId == null) {
            throw IllegalStateException("The ad unit ID must be set on NativeAdvancedUnitId before loadAd is called.")
        }
        val builder = AdLoader.Builder(context, adNativeAdvancedUnitId)

        var params = RelativeLayout.LayoutParams(
            RelativeLayout.LayoutParams.FILL_PARENT,
            dimension(190).toInt()
        )
        var loadingView = LayoutInflater.from(context).inflate(R.layout.ad_loading_layout, null)
        var avLoadingIndicatorView =
            loadingView.findViewById<AVLoadingIndicatorView>(R.id.ad_loading_view)
        if (avLoadingIndicatorView != null) {
            avLoadingIndicatorView!!.setIndicator(
                NativeAdvancedAd(context!!).loadingAnimationType[Random().nextInt(
                    NativeAdvancedAd(context!!).loadingAnimationType.size
                )]
            )
        }
        unitAdView.removeAllViews()
        unitAdView.addView(loadingView, params)

        when (displayMode) {
            AdDisplayMode.APP_INSTALL -> {
                builder.forAppInstallAd { nativeAppInstallAd ->
                    LogDebug.d("forAppInstallAd", "load native ad")
                    val nativeAppInstallAdView = layoutInflater.inflate(
                        R.layout.ad_app_install,
                        null
                    ) as NativeAppInstallAdView
                    populateAppInstallAdView(nativeAppInstallAd, nativeAppInstallAdView)
                    unitAdView.removeAllViews()
                    unitAdView.addView(nativeAppInstallAdView)


                }
            }
            AdDisplayMode.CONTENT -> {
                builder.forContentAd { nativeContentAd ->
                    val nativeContentAdView =
                        layoutInflater.inflate(R.layout.ad_content, null) as NativeContentAdView
                    populateContentAdView(nativeContentAd, nativeContentAdView)
                    unitAdView.removeAllViews()
                    unitAdView.addView(nativeContentAdView)
                }
            }
            AdDisplayMode.UNIFIED_NATIVE -> {
                builder.forUnifiedNativeAd { unifiedNativeAd ->
                    var colorDrawable =
                        ColorDrawable(ContextCompat.getColor(context!!, R.color.white))
                    val nativeContentAdView =
                        layoutInflater.inflate(layoutRes, null) as TemplateView
                    val template: TemplateView = nativeContentAdView.findViewById(R.id.my_template)
                    val styles = NativeTemplateStyle.Builder()
                        .withMainBackgroundColor(colorDrawable)
                        .build()
                    template.setStyles(styles)
                    template.setNativeAd(unifiedNativeAd)
                    unitAdView.removeAllViews()
                    unitAdView.addView(nativeContentAdView)
                    this.unifiedNativeAd = template
                }
            }
            else -> {
                null
            }
        }

        val videoOptions = VideoOptions.Builder()
            .setStartMuted(true)
            .build()

        var adOptions = NativeAdOptions.Builder()
            .setVideoOptions(videoOptions)
            .build()

        builder.withNativeAdOptions(adOptions)
        builder.withAdListener(object : AdListener() {

            override fun onAdLoaded() {
                super.onAdLoaded()
                adListener?.onAdLoaded()
                unitAdView.visibility = View.VISIBLE
                avLoadingIndicatorView?.visibility = View.GONE
            }

            override fun onAdOpened() {
                super.onAdOpened()
                adListener?.onAdOpened()
            }

            override fun onAdClosed() {
                super.onAdClosed()
                adListener?.onAdClosed()
            }

            override fun onAdClicked() {
                super.onAdClicked()
                adListener?.onAdClicked()
            }

            override fun onAdFailedToLoad(errorCode: Int) {
                super.onAdFailedToLoad(errorCode)
                adListener?.onAdFailedToLoad(errorCode)
                LogDebug.d("AdFailedToLoad", "Failed to load native ad: $errorCode")
                unitAdView.visibility = View.GONE
                avLoadingIndicatorView?.visibility = View.GONE
            }

            override fun onAdImpression() {
                super.onAdImpression()
                adListener?.onAdImpression()
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                adListener?.onAdLeftApplication()
            }
        })
        builder.build().loadAd(adRequest)
    }


    fun resume() {
        if (this.adViewBanner != null) {
            this.adViewBanner!!.resume()
        }
    }

    fun pause() {
        if (this.adViewBanner != null) {
            this.adViewBanner!!.pause()
        }
    }

    fun destroy() {
        if (this.adViewBanner != null) {
            this.adViewBanner!!.destroy()
        }
        nativeAppInstallAd?.destroy()
        nativeContentAd?.destroy()
        unifiedNativeAd?.destroyNativeAd()
    }

    companion object {
        private var TAG = AdMobHandler::class.java.name
        private var initError = "is not initialized yet"
        var adTestAppUnitId = "ca-app-pub-3940256099942544~3347511713"
        var adTestBannerId = "ca-app-pub-3940256099942544/6300978111"
        var adTestInterstitialId = "ca-app-pub-3940256099942544/1033173712"
        var adTestRewardedVideoId = "ca-app-pub-3940256099942544/5224354917"
        var adTestNativeAdvancedId = "ca-app-pub-3940256099942544/2247696110"
        var adTestNativeAdvancedVideoId = "ca-app-pub-3940256099942544/1044960115"

        var mAdMobHandler: AdMobHandler? = null

        @Synchronized
        fun getInstance(context: Context?, adRequest: AdRequest): AdMobHandler? {
            synchronized(AdMobHandler::class.java) {
                if (mAdMobHandler == null) {
                    return AdMobHandler(context, adRequest)
                }
            }
            return mAdMobHandler
        }
    }

    //TODO  Builder class
    class Builder {
        private var context: Context? = null
        private var mBannerSize: CustomAdSize? = null
        private var mAdSize: AdSize? = null
        private var mAppUnitId: String? = null
        private var mAdViewUnitId: String? = null
        private var mInterstitialUnitId: String? = null
        private var mRewardedVideoUnitId: String? = null
        private var mNativeAdvancedUnitId: String? = null
        private var mAdRequest: AdRequest? = null
        private var isDebugTestAds: Boolean = false

        fun setContext(context: Context?): Builder {
            this.context = context
            return this
        }

        fun setAppUnitId(adUnitId: String): Builder {
            this.mAppUnitId = adUnitId
            return this
        }

        fun setAdViewUnitId(adUnitId: String): Builder {
            this.mAdViewUnitId = adUnitId
            return this
        }

        fun setInterstitialUnitId(adUnitId: String): Builder {
            this.mInterstitialUnitId = adUnitId
            return this
        }

        fun setRewardedVideoUnitId(adUnitId: String): Builder {
            this.mRewardedVideoUnitId = adUnitId
            return this
        }

        fun setNativeAdvancedUnitId(adUnitId: String): Builder {
            this.mNativeAdvancedUnitId = adUnitId
            return this
        }

        fun setAdViewSize(adSize: AdSize?): Builder {
            this.mAdSize = adSize
            return this
        }

        private fun setAdRequest(adRequest: AdRequest?): Builder {
            this.mAdRequest = adRequest
            return this
        }

        fun setDebugTestAds(debug: Boolean): Builder {
            this.isDebugTestAds = debug
            return this
        }

        fun build(): AdMobHandler {
            var advertiseHandler =
                AdMobHandler(context, UserConsent.getInstance(context).getAdMobRequest())
            advertiseHandler.setAppUnitId(mAppUnitId)
            advertiseHandler.setAdViewUnitId(mAdViewUnitId)
            advertiseHandler.setInterstitialUnitId(mInterstitialUnitId)
            advertiseHandler.setRewardedVideoUnitId(mRewardedVideoUnitId)
            advertiseHandler.setNativeAdvancedUnitId(mNativeAdvancedUnitId)
            advertiseHandler.setAdViewSize(mAdSize)
            advertiseHandler.isDebugTestAds = isDebugTestAds
            return advertiseHandler
        }
    }
}