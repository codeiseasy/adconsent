package com.google.android.ads.consent.preferences

import android.content.Context
import android.content.SharedPreferences

class UserConsentPreferences(val context: Context?) {
    var preferences: SharedPreferences? = null
    private var dateSharedPreferences = "date_shared_preferences"
    private var isPersonalizedDatePrefKey = "is_personalized_date_pref_key"
    private var nonPersonalizedDatePrefKey = "non_personalized_date_pref_key"

    init {
        preferences = context!!.getSharedPreferences(dateSharedPreferences, Context.MODE_PRIVATE)
    }

    companion object {
        fun getInstance(context: Context?) : UserConsentPreferences {
            return UserConsentPreferences(context)
        }
    }

    fun setUserConsentPersonalizedDate(date: String){
        preferences!!.edit().putString(isPersonalizedDatePrefKey, date).apply()
    }

    fun setUserConsentNonPersonalizedDate(date: String){
        preferences!!.edit().putString(nonPersonalizedDatePrefKey, date).apply()
    }

    fun getUserConsentPersonalizedDate(): String{
        return preferences!!.getString(isPersonalizedDatePrefKey, "---").toString()
    }

    fun getUserConsentNonPersonalizedDate(): String{
        return preferences!!.getString(nonPersonalizedDatePrefKey, "---").toString()
    }
}