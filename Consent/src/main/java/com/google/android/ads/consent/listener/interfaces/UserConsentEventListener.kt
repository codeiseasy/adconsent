package com.google.android.ads.consent.listener.interfaces

import com.google.android.ads.consent.ConsentStatus

interface UserConsentEventListener {
    fun onResult(consentStatus: ConsentStatus)
    fun onFailed(reason: String)
}
