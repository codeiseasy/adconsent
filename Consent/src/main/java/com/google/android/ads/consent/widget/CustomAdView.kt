package com.google.android.ads.consent.widget

import android.content.Context
import android.graphics.Color

import android.util.AttributeSet
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.StringRes
import com.google.android.ads.consent.R
import com.google.android.ads.consent.listener.abstracts.AdMobListener
import com.google.android.ads.consent.util.LogDebug

import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView


class CustomAdView : RelativeLayout {
    private var mTAG = CustomAdView::class.java.name
    private var mAdView: AdView? = null


    private var mAdMobListener: AdMobListener? = null

    private var mAdUnitId: String? = null
    private var mBannerSize: AdSize? = null

    constructor(context: Context?) : super(context) {
        //initTypedArray(context, null)
        initAd()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        //initTypedArray(context, attrs)
        initAd()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        //initTypedArray(context, attrs)
        initAd()
    }

    private fun initTypedArray(context: Context?, attrs: AttributeSet?){
        val typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.CustomAdView, 0, 0)
        try {
            var type = typedArray.getInt(R.styleable.CustomAdView_unitSize, 0)
            var unitId = context!!.resources.getString(typedArray.getResourceId(R.styleable.CustomAdView_unitId, 0))
            if (unitId != null){
                mAdUnitId = unitId
            }
            //LogDebug.d("AdUnitId", mAdUnitId)
            setAdSize(type)
        } finally {
            typedArray.recycle()
        }
    }

    private fun initAd(){
        mAdView = AdView(context)
        var params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        layoutParams = params
        mAdView!!.layoutParams = params
        //demoView()
        addView(mAdView)
    }
    fun demoView(): View{
        var demoLayout = LinearLayout(context)
        demoLayout.gravity = Gravity.CENTER
        //demoLayout.setPadding(50,50,50,50)
        demoLayout.setBackgroundColor(Color.parseColor("#D6D7D7"))
        var demoText = TextView(context)
        demoText.text = "Ads by Google"
        demoText.gravity = Gravity.CENTER
        var demoLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50f, context.resources.displayMetrics).toInt())
        demoLayout.layoutParams = demoLayoutParams

        var demoTextParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        demoText.layoutParams = demoTextParams

        demoLayout.addView(demoText)
        addView(demoLayout)
        return demoLayout
    }


    fun loadAd(adRequest: AdRequest?, unitId: String?, size: AdSize?){
        mAdView!!.adUnitId = unitId
        mAdView!!.adSize = size
        mAdView!!.loadAd(adRequest)
        mAdView!!.adListener = object : AdListener() {
            override fun onAdLoaded() {
                super.onAdLoaded()
                mAdMobListener?.onAdLoaded()
                LogDebug.d(mTAG, "[loadCustomAdView(){\nonAdLoaded()}]")

            }

            override fun onAdOpened() {
                super.onAdOpened()
                mAdMobListener?.onAdOpened()
                LogDebug.d(mTAG, "[loadCustomAdView(){\nonAdOpened()}]")
            }

            override fun onAdClicked() {
                super.onAdClicked()
                mAdMobListener?.onAdClicked()
                LogDebug.d(mTAG, "[loadCustomAdView(){\nonAdClicked()}]")
            }

            override fun onAdClosed() {
                super.onAdClosed()
                mAdMobListener?.onAdClosed()
                LogDebug.d(mTAG, "[loadCustomAdView(){\nonAdClosed()}]")
            }

            override fun onAdImpression() {
                super.onAdImpression()
                mAdMobListener?.onAdImpression()
                LogDebug.d(mTAG, "[loadCustomAdView(){\nonAdImpression()}]")
            }

            override fun onAdLeftApplication() {
                super.onAdLeftApplication()
                mAdMobListener?.onAdLeftApplication()
                LogDebug.d(mTAG, "[loadCustomAdView(){\nonAdLeftApplication()}]")
            }

            override fun onAdFailedToLoad(i: Int) {
                super.onAdFailedToLoad(i)
                mAdMobListener?.onAdFailedToLoad(i)
                LogDebug.d(mTAG, "[loadCustomAdView(){\nonAdFailedToLoad($i)}]")
            }
        }
    }

    fun setAdUnitId(unitId: String?){
        this.mAdUnitId = unitId
        mAdView!!.adUnitId = mAdUnitId
    }

    fun setAdUnitId(@StringRes unitId: Int){
        this.mAdUnitId = context.resources.getString(unitId)
        mAdView!!.adUnitId = mAdUnitId
    }

    private fun setAdSize(size: Int?) {
        mBannerSize = when (size!!) {
            1 -> AdSize.BANNER
            2 -> AdSize.SMART_BANNER
            3 -> AdSize.LARGE_BANNER
            4 -> AdSize.FULL_BANNER
            5 -> AdSize.MEDIUM_RECTANGLE
            else -> {
                AdSize.BANNER
            }
        }
    }

    fun setAdViewSize(size: AdSize?) {
        mBannerSize = size
        mAdView!!.adSize = mBannerSize
    }

    fun resume() {
        if (this.mAdView != null) {
            this.mAdView!!.resume()
        }
    }

    fun pause() {
        if (this.mAdView != null) {
            this.mAdView!!.pause()
        }
    }

    fun destroy() {
        if (this.mAdView != null) {
            this.mAdView!!.destroy()
        }
    }
}
