package com.google.android.ads.consent.listener.enums;

public enum CustomAdSize {
    BANNER,
    SMART_BANNER,
    LARGE_BANNER,
    FULL_BANNER,
    MEDIUM_RECTANGLE,
}
