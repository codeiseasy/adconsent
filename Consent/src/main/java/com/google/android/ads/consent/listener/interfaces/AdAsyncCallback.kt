package com.google.android.ads.consent.listener.interfaces

interface AdAsyncCallback {
    fun onNext()
}