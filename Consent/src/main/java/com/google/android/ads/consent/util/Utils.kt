package com.google.android.ads.consent.util

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import ma.allovacances.app.util.FontUtil
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.drawable.Drawable

import android.graphics.Bitmap
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter

import androidx.annotation.ColorInt


object Utils {

    fun checkLinkExt(url: String): Boolean {
        return when {
            url.startsWith("http://") -> true
            url.startsWith("https://") -> true
            else -> false
        }
    }

    fun stripNonDigits(input: CharSequence): String {
        val sb = StringBuilder(input.length)
        for (i in 0 until input.length) {
            val c = input[i]
            if (c.toInt() in 48..57) {
                sb.append(c)
            }
        }

        return sb.toString()
    }


    fun openBrowser(context: Context?, url: String?) {
        try {
            context!!.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

     fun getCurrentDate(): String {
        // (1) get today's date
        var today = Calendar.getInstance().time
        // (2) create a date "formatter" (the date format we want)
        var formatter =  SimpleDateFormat("dd/MM/yyyy hh:mm:ss")
        // (3) create a new String using the date format we want
        return formatter.format(today)
    }


    fun changeStatusBarColor(context: Activity) {
        if (Build.VERSION.SDK_INT >= 21) {
            val window = context.window
            window.addFlags(Integer.MIN_VALUE)
            window.statusBarColor = 0
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }


    fun deepChangeTextColor(ctx: Context?, parentLayout: ViewGroup, font: String) {
        for (count in 0 until parentLayout.childCount) {
            val view = parentLayout.getChildAt(count)
            when (view) {
                is TextView -> //view.textSize = 16f
                    FontUtil.overrideFonts(ctx, view, font)
                is Button -> // view.textSize = 16f
                    FontUtil.overrideFonts(ctx, view, font)
                is ViewGroup -> deepChangeTextColor(ctx, view, font)
            }
        }
    }


    fun applyThemeToDrawable(image: Drawable?, @ColorInt color: Int) {
        if (image != null) {
            val porterDuffColorFilter = PorterDuffColorFilter(
                color,
                PorterDuff.Mode.SRC_ATOP
            )

            image.colorFilter = porterDuffColorFilter
        }
    }

    fun setImageColorFilters(context: Context?, imageView: ImageView?, colors: IntArray){
        val drawable = (imageView as ImageView).drawable
        val bitmap = (drawable as BitmapDrawable).bitmap

        //var myBitmap = (imageView?.drawable as BitmapDrawable).bitmap


       // var newBitmap = addGradient(myBitmap,colors)
       // imageView?.setImageDrawable(BitmapDrawable(context!!.resources, newBitmap))
    }


    fun setTextColorFilters(textView: TextView, text: String, colors: IntArray){
        val paint = textView.paint
        val width = paint.measureText(text)

        val textShader = LinearGradient(0f, 0f, width, textView.textSize,
            colors, null, Shader.TileMode.CLAMP
        )
        textView.paint.shader = textShader
    }

    private fun addGradient(originalBitmap: Bitmap, colors: IntArray): Bitmap {
        val width = originalBitmap.width
        val height = originalBitmap.height
        val updatedBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(updatedBitmap)
        canvas.drawBitmap(originalBitmap, 0f, 0f, null)

        val paint = Paint()
        val shader = LinearGradient(0f, 0f, 0f, height.toFloat(), colors, null, Shader.TileMode.CLAMP)
        paint.shader = shader
        paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), paint)

        return updatedBitmap
    }


    fun defaultColors(): IntArray{
        return  intArrayOf(
            Color.parseColor("#F97C3C"),
            Color.parseColor("#FDB54E"),
            Color.parseColor("#64B678"),
            Color.parseColor("#478AEA"),
            Color.parseColor("#8446CC")
        )
    }

    fun colors(): IntArray{
        //-0xf2dae, -0xf8cfb
        return intArrayOf(
            Color.parseColor("#FF5500"),
            Color.parseColor("#FF2525"),
            Color.parseColor("#F5182A"),
            Color.parseColor("#BB5A2C"),
            Color.parseColor("#DC4E41")
        )
    }

    fun colors(alpha: Int, colors: IntArray): IntArray{
        //-0xf2dae, -0xf8cfb
        return colors
    }

    /**
     * @param colorCode color, without alpha
     * @param percentage         from 0.0 to 1.0
     * @return
     */
    fun setColorAlpha(colorCode: String, percentage: Int): String {
        val decValue = percentage.toDouble() / 100 * 255
        val rawHexColor = colorCode.replace("#", "")
        val str = StringBuilder(rawHexColor)

        if (Integer.toHexString(decValue.toInt()).length == 1)
            str.insert(0, "#0" + Integer.toHexString(decValue.toInt()))
        else
            str.insert(0, "#" + Integer.toHexString(decValue.toInt()))
        return str.toString()
    }

    /**
     * @param originalColor color, without alpha
     * @param alpha         from 0.0 to 1.0
     * @return
     */
    fun addAlpha(originalColor: String, alpha: Double): String {
        var originalColor = originalColor
        val alphaFixed = Math.round(alpha * 255)
        var alphaHex = java.lang.Long.toHexString(alphaFixed)
        if (alphaHex.length == 1) {
            alphaHex = "0$alphaHex"
        }
        originalColor = originalColor.replace("#", "#$alphaHex")


        return originalColor
    }

}
