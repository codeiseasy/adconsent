package com.google.android.ads.consent.listener.abstracts

import com.google.android.ads.consent.ConsentInformation
import com.google.android.ads.consent.ConsentStatus

abstract class UserConsentInformationCallback {
    abstract fun onResult(consentInformation: ConsentInformation, consentStatus: ConsentStatus)
    abstract fun onFailed(consentInformation: ConsentInformation, reason: String)
}