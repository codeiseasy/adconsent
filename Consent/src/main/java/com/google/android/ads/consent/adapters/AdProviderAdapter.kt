package com.google.android.ads.consent.adapters

import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView


import com.google.android.ads.consent.models.AdProviders
import com.google.android.ads.consent.util.SpannableText

import com.google.android.ads.consent.widget.VectorDrawableShape

class AdProviderAdapter(private var providers: ArrayList<AdProviders>, private val color: Int) : BaseAdapter() {

    class MyHolder {
        var providerName: TextView? = null
        var providerLink: TextView? = null
        var providerBorderLayout: LinearLayout? = null
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var convertView = convertView
        var holder: MyHolder? = null
        if (convertView == null){
            convertView = LayoutInflater.from(parent!!.context).inflate(com.google.android.ads.consent.R.layout.provider_items, null)
            holder = MyHolder()
            convertView.tag = holder
        } else {
            holder = convertView.tag as MyHolder
        }

        holder.providerBorderLayout = convertView?.findViewById(com.google.android.ads.consent.R.id.provider_border_layout)
        holder.providerName = convertView?.findViewById(com.google.android.ads.consent.R.id.provider_name)
        holder.providerLink = convertView?.findViewById(com.google.android.ads.consent.R.id.provider_link)


        var provider = providers[position]
        var name = provider.getName()

        holder.providerName?.text = name
        holder.providerName?.setTextColor(color)

        var link = "<a href=" + provider.getLink() + ">" + "View" + "</a>"
        holder.providerLink?.setOnClickListener { v-> v.invalidate() }

        holder.providerLink?.text = Html.fromHtml(link)
        holder.providerLink?.setLinkTextColor(color) // ff533d
        SpannableText.stripUnderlines(holder.providerLink!!)
        holder.providerLink?.movementMethod = LinkMovementMethod.getInstance()

        holder.providerBorderLayout?.setBackgroundDrawable(VectorDrawableShape.setupBorderDrawable().setCornerRadius(5f).setStroke(1, color).apply())
        return convertView!!
    }

    override fun getItem(position: Int): Any {
        return providers[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return providers?.size
    }
}