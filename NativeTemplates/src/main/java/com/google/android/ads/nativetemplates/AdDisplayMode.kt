package com.google.android.ads.nativetemplates

enum class AdDisplayMode {
    APP_INSTALL,
    CONTENT,
    UNIFIED_NATIVE
}