package com.google.android.ads.nativetemplates

import android.graphics.Color

class NativeOptions {
    var nativeBackgroundColor: Int = Color.WHITE
    var headlineColor: Int = Color.GRAY
    var bodyColor: Int = Color.GRAY
    var storeColor: Int = Color.GRAY
    var advertiserColor: Int = Color.GRAY
    var callToActionBackgroundColor: Int = Color.BLUE
    var callToActionTextColor: Int = Color.WHITE
    var callToActionTextSize: Int = 0
}