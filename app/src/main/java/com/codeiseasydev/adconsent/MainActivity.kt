package com.codeiseasydev.adconsent

import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import androidx.core.content.ContextCompat
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.codeiseasydev.adconsent.Constants.privacyPolicy
import com.codeiseasydev.adconsent.Constants.publisherId
import com.google.android.ads.consent.core.AdMobHandler
import com.google.android.ads.consent.listener.abstracts.AdMobListener
import com.google.android.ads.consent.listener.interfaces.AdRewardedListener
import com.google.android.ads.consent.ui.UserConsent
import com.google.android.ads.consent.ui.UserConsentSettingsActivity
import com.google.android.ads.nativetemplates.AdDisplayMode
import com.google.android.ads.nativetemplates.AdDisplaySize
import com.google.android.ads.nativetemplates.NativeTemplateStyle
import com.google.android.ads.nativetemplates.TemplateView
import com.google.android.gms.ads.*
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var adMobHandler: AdMobHandler? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        adMobHandler = AdMobHandler.Builder()
            .setContext(this)
            .setDebugTestAds(true)
            .setAppUnitId(resources.getString(R.string.test_admob_app_unit_id))
            .setAdViewUnitId(resources.getString(R.string.test_admob_banner_unit_id))
            .setInterstitialUnitId(resources.getString(R.string.test_admob_interstitial_unit_id))
            .setRewardedVideoUnitId(resources.getString(R.string.test_admob_rewarded_video_unit_id))
            .setNativeAdvancedUnitId(resources.getString(R.string.test_admob_native_advance_unit_id))
            .setAdViewSize(AdSize.BANNER)
            .build()

        //loadAdView()
        loadNativeAd()

        btn_load_interstitial.setOnClickListener {
            adMobHandler?.loadInterstitialAd()
            //adMobHandler?.loadInterstitialAfterCountOf(it.id.toString(), 2)
            return@setOnClickListener
            adMobHandler?.loadInterstitialAfterCountOf(
                it.id.toString(),
                2,
                object : AdMobListener() {
                    override fun onAdClosed() {
                        super.onAdClosed()
                        Toast.makeText(this@MainActivity, it.id.toString(), Toast.LENGTH_LONG)
                            .show()
                    }
                })
        }

        btn_load_rewarded_video.setOnClickListener {
            adMobHandler?.loadAdRewardedVideo()
            //adMobHandler?.loadRewardedVideoAfterCountOf(it.id.toString(), 2)
            return@setOnClickListener

            adMobHandler?.loadRewardedVideoAfterCountOf(
                it.id.toString(),
                2,
                object : AdRewardedListener() {
                    override fun onRewardedVideoAdClosed() {
                        super.onRewardedVideoAdClosed()
                        Toast.makeText(this@MainActivity, it.id.toString(), Toast.LENGTH_LONG)
                            .show()
                    }
                })
        }

        btn_setting_consent.setOnClickListener {
            UserConsentSettingsActivity.start(this, privacyPolicy, publisherId)
        }

        btn_revoke_consent.setOnClickListener {
            UserConsent.getInstance(this).revokeUserConsentDialog()
        }
    }


    private fun loadAdView() {
        adMobHandler?.loadAdView(adView, null)
    }

    private fun loadNativeAd() {
        //adMobHandler?.loadNativeAd(adView, AdDisplaySize.SMALL)
        adMobHandler?.loadNativeAd(adView, AdDisplayMode.APP_INSTALL)
        //adMobHandler?.loadNativeForAppInstallAd(adView)
        return
        //var nativeAdView = findViewById<NativeAdView>(R.id.ad_view)
        MobileAds.initialize(this, resources.getString(R.string.test_admob_app_unit_id))
        var adLoader =
            AdLoader.Builder(this, resources.getString(R.string.test_admob_native_advance_unit_id))
                .forUnifiedNativeAd { unifiedNativeAd ->
                    var styles = NativeTemplateStyle.Builder()
                        .withMainBackgroundColor(
                            ColorDrawable(
                                ContextCompat.getColor(
                                    this,
                                    R.color.white
                                )
                            )
                        )
                        .withCallToActionBackgroundColor(
                            ColorDrawable(
                                ContextCompat.getColor(
                                    this,
                                    R.color.colorAccent
                                )
                            )
                        )
                        .withCallToActionTextSize(16f)
                        .withPrimaryTextSize(16f)
                        .build()

                    var template = findViewById<TemplateView>(R.id.my_template)
                    template.setStyles(styles)
                    template.setNativeAd(unifiedNativeAd)
                    //nativeAdView.setNativeAd(unifiedNativeAd)
                }
                .build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    override fun onPause() {
        super.onPause()
        adMobHandler?.pause()
    }

    override fun onResume() {
        super.onResume()
        adMobHandler?.resume()
    }

    override fun onDestroy() {
        adMobHandler?.destroy()
        super.onDestroy()
    }
}
