package com.codeiseasydev.adconsent


import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import com.codeiseasydev.adconsent.Constants.privacyPolicy
import com.codeiseasydev.adconsent.Constants.publisherId
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.enums.WindowMode
import com.google.android.ads.consent.listener.enums.AdContentRating
import com.google.android.ads.consent.listener.interfaces.UserConsentEventListener
import com.google.android.ads.consent.ui.UserConsent
import com.google.android.ads.consent.ui.UserConsentDialogStyle

import com.google.android.gms.ads.AdRequest


open class UserConsentActivity : AppCompatActivity() {
    private var userConsent: UserConsent? = null

    interface OnUserConsentCallback {
        fun onUserConsentResult(consentStatus: ConsentStatus)
        fun onUserConsentError(reason: String)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        var style = UserConsentDialogStyle(this)
        style.setDisplayIcon(false)
        style.setBtnPersonalizedIAgreeText("Yes, I Accept")
        style.setBtnPersonalizedDisagreeText("No, Thanks")
        style.setBtnPersonalizedAgreeText("Accept")
        style.setTopOfDialogTitleColor(resources.getColor(R.color.white)) //606060
        style.setTopOfDialogBackgroundColor(resources.getColor(R.color.colorPrimary)) //F0F0F0
        style.setBtnBackgroundColor(resources.getColor(R.color.colorPrimaryDark))
        style.setBtnCornerRadius(4)
        //style.setDialogCornerRadius(8)


        userConsent = UserConsent.Builder()
            .setContext(this)
            .setCaliforniaConsumerPrivacyAct(true)
            .setPrivacyUrl(privacyPolicy) // Add your privacy policy url
            .setPublisherId(publisherId) // Add your admob publisher id
            .setTestDeviceId(AdRequest.DEVICE_ID_EMULATOR) // Add your test device id "Remove addTestDeviceId on production!"
            .setLog(UserConsentActivity::class.java.name) // Add custom tag default: ID_LOG
            .setDebugGeography(true) // Geography appears as in EEA for test devices.
            //.setTagForChildDirectedTreatment(true) // setTagForChildDirectedTreatment to true to indicate that you want your content treated as child-directed for purposes of COPPA.
            .setTagForUnderAgeOfConsent(true) //setTagForUnderAgeOfConsent to true to indicate that you want the ad request to be handled in a manner suitable for users under the age of consent.
            .setShowingWindowWithAnimation(true)
            .setShowingWindowMode(WindowMode.SHEET_BOTTOM)
            .setUserConsentStyle(style)
            .setMaxAdContentRating(AdContentRating.MA)
            .build()
    }

    fun checkUserConsent(callback: OnUserConsentCallback?) {
        userConsent?.checkUserConsent(object : UserConsentEventListener {
            override fun onResult(consentStatus: ConsentStatus) {
                callback?.onUserConsentResult(consentStatus)
            }

            override fun onFailed(reason: String) {
                callback?.onUserConsentError(reason)
            }
        })
    }

    fun revokeUserConsent() {
        userConsent?.revokeUserConsent()
    }

}