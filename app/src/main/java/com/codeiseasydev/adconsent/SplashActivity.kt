package com.codeiseasydev.adconsent

import android.content.Intent
import android.os.Bundle
import com.google.android.ads.consent.ConsentStatus
import com.google.android.ads.consent.util.UserConsentSession

class SplashActivity : UserConsentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (UserConsentSession.getInstance(this@SplashActivity).isUserConfirmAgreement()){
             goToMain()
        } else {
            checkUserConsent(object : OnUserConsentCallback {
                override fun onUserConsentResult(consentStatus: ConsentStatus) {
                    UserConsentSession.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                    goToMain()
                }

                override fun onUserConsentError(reason: String) {
                    UserConsentSession.getInstance(this@SplashActivity).setUserConfirmAgreement(true)
                    goToMain()
                }
            })
        }
    }

    fun goToMain(){
        startActivity(Intent(this@SplashActivity, MainActivity::class.java))
        finish()
    }
}